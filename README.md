
---

**WARNING!**: This is the *Old* source-code repository for FastaTools program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/fastatools/) located at https://sourceforge.net/p/lp-csic-uab/fastatools/**  

---  
  
  
![http://lh4.ggpht.com/_ty9pOjnnHDA/TOUXScT5WnI/AAAAAAAAAHI/WJUtCilq5dM/fastatools2.jpg](http://lh4.ggpht.com/_ty9pOjnnHDA/TOUXScT5WnI/AAAAAAAAAHI/WJUtCilq5dM/fastatools2.jpg)


---

**WARNING!**: This is the *Old* source-code repository for FastaTools program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/fastatools/) located at https://sourceforge.net/p/lp-csic-uab/fastatools/**  

---  
  
  
#### Description

**[FastaTools](http://sites.google.com/site/fastatools/)** is a GUI based, [Perl](http://www.perl.org/) made application that performs several operations to **[Fasta](http://en.wikipedia.org/wiki/FASTA_format) protein databases (DB)**.

**FastaTools** performs four main actions:

- **DB exploration**: it shows the first 200 lines of the database.

- **DB Analysis**: the number of lines, proteins and peptides generated after digestion by an enzyme are reported. Five different enzymes are supported at this time, and the user can specify the number of missed cleavages, minimum and maximum masses for peptide generation.

- **Decoy DB** generation: a decoy database is generated using the specified DB. Three strategies of decoy generation are supported (reverse, random and pseudo-random). The new DB can be attached to the original.

- **DB Search**: a simple text or a list of terms can be introduced to be searched inside the DB. The headers or the sequences can be scanned, into three general strategies:
  * Search headers to generate a sub-DB in fasta format.
  * Search sequences to generate a sub-DB in fasta format.
  * Search sequences to generate a peptide-protein pairs list.
  * Search sequences to generate a protein-peptide pairs list.

- **DB Join**: join multiple Fasta databases.

#### Download

You can download the last version of FastaTools [here](https://bitbucket.org/lp-csic-uab/fastatools/downloads)[![](https://lh6.googleusercontent.com/-LQE2us7J9GI/TnMstHYmquI/AAAAAAAAAKU/HgdPvan2S08/s800/downloadicon.jpg)](https://bitbucket.org/lp-csic-uab/fastatools/downloads).

After downloading the binary installer, you have to e-mail us at ![https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png](https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png) to get your free password and unlock the installation program.

#### Documentation

More documentation and tutorials can be downloaded as a **[PDF file](https://bitbucket.org/lp-csic-uab/fastatools/downloads/FastaTools%20Documentation%20and%20Tutorials.pdf)**.

![https://lh4.googleusercontent.com/-Bt7W-jMY4yo/TnxxV2XonjI/AAAAAAAAAN8/Dljqrap5npM/s800/snapshoot_fastatools.jpg](https://lh4.googleusercontent.com/-Bt7W-jMY4yo/TnxxV2XonjI/AAAAAAAAAN8/Dljqrap5npM/s800/snapshoot_fastatools.jpg)



**Copyright notice:** This software has been produced at the [Proteomics Facility CSIC-UAB](http://proteomica.uab.cat) by _David Ovelleiro_ on the framework of a Spanish BIO2004/01788 directed by _Dr. Joaquin Abian_. It's under the [GNU General Public License (v3)](http://www.gnu.org/licenses/gpl.html) and the source code and binaries are available through the [main Project Page](http://code.google.com/p/lp-csic-uab/) at http://lp-csic-uab.googlecode.com .


---

**WARNING!**: This is the *Old* source-code repository for FastaTools program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/fastatools/) located at https://sourceforge.net/p/lp-csic-uab/fastatools/**  

---  
  
  
