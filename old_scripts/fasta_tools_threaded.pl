#!/usr/bin/perl
	#FastaTools version 0.9
	#Copyright (C) 2008  David Ovelleiro

    #This program is free software: you can redistribute it and/or modify
    #it under the terms of the GNU General Public License as published by
    #the Free Software Foundation, either version 3 of the License, or
    #(at your option) any later version.

    #This program is distributed in the hope that it will be useful,
    #but WITHOUT ANY WARRANTY; without even the implied warranty of
    #MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    #GNU General Public License for more details.

    #You should have received a copy of the GNU General Public License
    #along with this program.  If not, see <http://www.gnu.org/licenses/>






package MyFrame;

use threads;
use threads::shared;
use Wx qw[:everything];
use base qw(Wx::Frame);
use Wx::Event qw(EVT_BUTTON);
use strict;

sub new {
	my( $self, $parent, $id, $title, $pos, $size, $style, $name ) = @_;
	$parent = undef              unless defined $parent;
	$id     = -1                 unless defined $id;
	$title  = ""                 unless defined $title;
	$pos    = wxDefaultPosition  unless defined $pos;
	$size   = wxDefaultSize      unless defined $size;
	$name   = ""                 unless defined $name;

# begin wxGlade: MyFrame::new

	$style = wxCAPTION|wxCLOSE_BOX|wxMINIMIZE_BOX|wxSYSTEM_MENU 
		unless defined $style;

	$self = $self->SUPER::new( $parent, $id, $title, $pos, $size, $style, $name );
	$self->{notebook_1} = Wx::Notebook->new($self, -1, wxDefaultPosition, wxDefaultSize, 0);
	$self->{notebook_1_pane_4} = Wx::Panel->new($self->{notebook_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{notebook_1_pane_3} = Wx::Panel->new($self->{notebook_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{notebook_1_pane_2} = Wx::Panel->new($self->{notebook_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{notebook_1_pane_1} = Wx::Panel->new($self->{notebook_1}, -1, wxDefaultPosition, wxDefaultSize, );
	

	# Menu Bar

	$self->{frame_1_menubar} = Wx::MenuBar->new();
	my $wxglade_tmp_menu;
	$wxglade_tmp_menu = Wx::Menu->new();
	$wxglade_tmp_menu->Append(1, "Exit", "");
	$self->{frame_1_menubar}->Append($wxglade_tmp_menu, "File");
	$wxglade_tmp_menu = Wx::Menu->new();
	$wxglade_tmp_menu->Append(2, "Help", "");
	$wxglade_tmp_menu->Append(3, "About", "");
	$self->{frame_1_menubar}->Append($wxglade_tmp_menu, "Help");
	$self->SetMenuBar($self->{frame_1_menubar});
	
# Menu Bar end

	$self->{frame_1_statusbar} = $self->CreateStatusBar(1, 0);
	$self->{panel_1} = Wx::Panel->new($self, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{button_3} = Wx::Button->new($self, -1, "Browse");
	$self->{text_ctrl_4} = Wx::TextCtrl->new($self, -1, "", wxDefaultPosition, wxDefaultSize, );
	$self->{panel_3} = Wx::Panel->new($self, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{button_4} = Wx::Button->new($self, -1, "Explore");
	$self->{panel_4} = Wx::Panel->new($self, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{text_ctrl_1} = Wx::TextCtrl->new($self, -1, "", wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxHSCROLL);
	$self->{panel_2} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_3_copy} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_3_copy_1} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_3_copy_2} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_3_copy_3} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_1} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{checkbox_1} = Wx::CheckBox->new($self->{notebook_1_pane_1}, -1, "", wxDefaultPosition, wxDefaultSize, );
	$self->{label_1} = Wx::StaticText->new($self->{notebook_1_pane_1}, -1, "   Number of proteins", wxDefaultPosition, wxDefaultSize, );
	$self->{button_1} = Wx::Button->new($self->{notebook_1_pane_1}, -1, "Analyze Database");
	$self->{panel_3_copy_2_copy} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_3_copy_7} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_2} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{checkbox_1_copy} = Wx::CheckBox->new($self->{notebook_1_pane_1}, -1, "", wxDefaultPosition, wxDefaultSize, );
	$self->{label_1_copy} = Wx::StaticText->new($self->{notebook_1_pane_1}, -1, "   Amino acid frequencies", wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_10} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_15} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_3_copy_11} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_3} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{checkbox_1_copy_1} = Wx::CheckBox->new($self->{notebook_1_pane_1}, -1, "", wxDefaultPosition, wxDefaultSize, );
	$self->{label_1_copy_1} = Wx::StaticText->new($self->{notebook_1_pane_1}, -1, "   Number of fragments", wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_11} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_12} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_13} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_3_copy} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_3_copy_1} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{label_1_copy_1_copy} = Wx::StaticText->new($self->{notebook_1_pane_1}, -1, "                      Enzyme", wxDefaultPosition, wxDefaultSize, );
	$self->{combo_box_1} = Wx::ComboBox->new($self->{notebook_1_pane_1}, -1, "", wxDefaultPosition, wxDefaultSize, ["Trypsin (KR)", "Trypsin (KR|P)", "GluC (DE)", "GluC (E)", "Endolysin"], wxCB_DROPDOWN);
	$self->{panel_2_copy_3_copy_1_copy_2} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_3_copy_1_copy_3} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_3_copy_1_copy} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_3_copy_1_copy_1} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{label_1_copy_1_copy_copy} = Wx::StaticText->new($self->{notebook_1_pane_1}, -1, "       Missed cleavages     ", wxDefaultPosition, wxDefaultSize, );
	$self->{text_ctrl_3} = Wx::TextCtrl->new($self->{notebook_1_pane_1}, -1, "2", wxDefaultPosition, wxDefaultSize, wxTE_CENTRE);
	$self->{panel_2_copy_3_copy_1_copy_4} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_3_copy_1_copy_5} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_3_copy_1_copy_4_copy_4} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_3_copy_1_copy_4_copy_5} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{label_1_copy_1_copy_copy_1} = Wx::StaticText->new($self->{notebook_1_pane_1}, -1, "                  Min. Mass   ", wxDefaultPosition, wxDefaultSize, );
	$self->{text_ctrl_3_copy} = Wx::TextCtrl->new($self->{notebook_1_pane_1}, -1, "600", wxDefaultPosition, wxDefaultSize, wxTE_CENTRE);
	$self->{panel_2_copy_3_copy_1_copy_4_copy} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_3_copy_1_copy_4_copy_2} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_3_copy_1_copy_4_copy_6} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_3_copy_1_copy_4_copy_7} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{label_1_copy_1_copy_copy_1_copy} = Wx::StaticText->new($self->{notebook_1_pane_1}, -1, "                  Max. Mass   ", wxDefaultPosition, wxDefaultSize, );
	$self->{text_ctrl_3_copy_1} = Wx::TextCtrl->new($self->{notebook_1_pane_1}, -1, "5000", wxDefaultPosition, wxDefaultSize, wxTE_CENTRE);
	$self->{panel_2_copy_3_copy_1_copy_4_copy_1} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_3_copy_1_copy_4_copy_3} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_3_copy_1_copy_4_copy_6_copy} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_3_copy_1_copy_4_copy_6_copy_1} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_3_copy_1_copy_4_copy_6_copy_2} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_3_copy_1_copy_4_copy_6_copy_3} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_3_copy_1_copy_4_copy_6_copy_4} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_2_copy_3_copy_1_copy_4_copy_6_copy_5} = Wx::Panel->new($self->{notebook_1_pane_1}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_5} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_5_copy} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_5_copy_1} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_5_copy_2} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_5_copy_3} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_5_copy_4} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_5_copy_5} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{label_2} = Wx::StaticText->new($self->{notebook_1_pane_2}, -1, "      Decoy type", wxDefaultPosition, wxDefaultSize, );
	$self->{combo_box_2} = Wx::ComboBox->new($self->{notebook_1_pane_2}, -1, "", wxDefaultPosition, wxDefaultSize, ["Reverse", "PseudoReverse Trypsin (KR|P)", "PseudoReverse Trypsin (KR)", "PseudoReverse GluC (DE)", "PseudoReverse Endolysin", "Random"], wxCB_DROPDOWN|wxCB_READONLY);
	$self->{panel_5_copy_6} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_5_copy_6_copy_4} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_5_copy_7} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_5_copy_8} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_6} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{button_2} = Wx::Button->new($self->{notebook_1_pane_2}, -1, "Generate Decoy");
	$self->{panel_5_copy_6_copy} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_5_copy_6_copy_1} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_5_copy_6_copy_2} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_5_copy_6_copy_3} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{label_2_copy_1} = Wx::StaticText->new($self->{notebook_1_pane_2}, -1, "Out name   ", wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
	$self->{text_ctrl_2} = Wx::TextCtrl->new($self->{notebook_1_pane_2}, -1, "decoyDB.fasta", wxDefaultPosition, wxDefaultSize, wxTE_CENTRE);
	$self->{panel_5_copy_6_copy_3_copy} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_5_copy_6_copy_3_copy_1} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_5_copy_6_copy_3_copy_2} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_5_copy_6_copy_3_copy_3} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_5_copy_6_copy_3_copy_3_copy} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{checkbox_2} = Wx::CheckBox->new($self->{notebook_1_pane_2}, -1, "Attach to the original DB ", wxDefaultPosition, wxDefaultSize, );
	$self->{panel_5_copy_6_copy_3_copy_6} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_5_copy_6_copy_3_copy_7} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_5_copy_6_copy_3_copy_8} = Wx::Panel->new($self->{notebook_1_pane_2}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_7} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_7_copy} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_7_copy_1} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_8} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_7_copy_3} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_7_copy_4} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_7_copy_5} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{label_3} = Wx::StaticText->new($self->{notebook_1_pane_3}, -1, " Mode   ", wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
	$self->{combo_box_3} = Wx::ComboBox->new($self->{notebook_1_pane_3}, -1, "", wxDefaultPosition, wxDefaultSize, ["1- search headers, generate sub-DB (fasta format)", "2- search sequences, generate sub-DB (fasta format)", "3- search  sequences, generate peptide-protein list", "4- search  sequences, generate protein-peptide list"], wxCB_DROPDOWN);
	$self->{panel_8_copy} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_8_copy_copy} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_7_copy_8} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_7_copy_9} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{label_3_copy} = Wx::StaticText->new($self->{notebook_1_pane_3}, -1, " Search: ", wxDefaultPosition, wxDefaultSize, );
	$self->{text_ctrl_5} = Wx::TextCtrl->new($self->{notebook_1_pane_3}, -1, "", wxDefaultPosition, wxDefaultSize, );
	$self->{button_5} = Wx::Button->new($self->{notebook_1_pane_3}, -1, "use list");
	$self->{panel_8_copy_copy_1} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_7_copy_11} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_7_copy_12} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_7_copy_12_copy} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{checkbox_3} = Wx::CheckBox->new($self->{notebook_1_pane_3}, -1, "  Use REGEX wildcards", wxDefaultPosition, wxDefaultSize, );
	$self->{panel_7_copy_12_copy_copy} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_7_copy_14} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_7_copy_15} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_7_copy_16} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{label_3_copy_copy} = Wx::StaticText->new($self->{notebook_1_pane_3}, -1, "Out Name ", wxDefaultPosition, wxDefaultSize, );
	$self->{text_ctrl_6} = Wx::TextCtrl->new($self->{notebook_1_pane_3}, -1, "sub_DB.fasta", wxDefaultPosition, wxDefaultSize, wxTE_CENTRE);
	$self->{panel_7_copy_12_copy_copy_copy} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_7_copy_18} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_7_copy_19} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_7_copy_20} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_7_copy_21} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{button_6} = Wx::Button->new($self->{notebook_1_pane_3}, -1, "Begin Search");
	$self->{panel_7_copy_12_copy_copy_copy_1} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_7_copy_24} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_7_copy_25} = Wx::Panel->new($self->{notebook_1_pane_3}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_9} = Wx::Panel->new($self->{notebook_1_pane_4}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_10} = Wx::Panel->new($self->{notebook_1_pane_4}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_10_copy} = Wx::Panel->new($self->{notebook_1_pane_4}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_10_copy_1} = Wx::Panel->new($self->{notebook_1_pane_4}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{label_4} = Wx::StaticText->new($self->{notebook_1_pane_4}, -1, "File A   +   File B   +  File C   =   File D", wxDefaultPosition, wxDefaultSize, );
	$self->{panel_10_copy_2} = Wx::Panel->new($self->{notebook_1_pane_4}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_10_copy_3} = Wx::Panel->new($self->{notebook_1_pane_4}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{label_5} = Wx::StaticText->new($self->{notebook_1_pane_4}, -1, "      A ", wxDefaultPosition, wxDefaultSize, );
	$self->{text_ctrl_7} = Wx::TextCtrl->new($self->{notebook_1_pane_4}, -1, "...", wxDefaultPosition, wxDefaultSize, );
	$self->{panel_11} = Wx::Panel->new($self->{notebook_1_pane_4}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{label_5_copy} = Wx::StaticText->new($self->{notebook_1_pane_4}, -1, "      B", wxDefaultPosition, wxDefaultSize, );
	$self->{text_ctrl_8} = Wx::TextCtrl->new($self->{notebook_1_pane_4}, -1, "", wxDefaultPosition, wxDefaultSize, );
	$self->{button_8} = Wx::Button->new($self->{notebook_1_pane_4}, -1, "Browse");
	$self->{label_5_copy_1} = Wx::StaticText->new($self->{notebook_1_pane_4}, -1, "      C ", wxDefaultPosition, wxDefaultSize, );
	$self->{text_ctrl_9} = Wx::TextCtrl->new($self->{notebook_1_pane_4}, -1, "", wxDefaultPosition, wxDefaultSize, );
	$self->{button_9} = Wx::Button->new($self->{notebook_1_pane_4}, -1, "Browse");
	$self->{panel_16} = Wx::Panel->new($self->{notebook_1_pane_4}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_16_copy} = Wx::Panel->new($self->{notebook_1_pane_4}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_16_copy_1} = Wx::Panel->new($self->{notebook_1_pane_4}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_10_copy_9} = Wx::Panel->new($self->{notebook_1_pane_4}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_10_copy_4} = Wx::Panel->new($self->{notebook_1_pane_4}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{label_5_copy_1_copy} = Wx::StaticText->new($self->{notebook_1_pane_4}, -1, "      D", wxDefaultPosition, wxDefaultSize, );
	$self->{text_ctrl_10} = Wx::TextCtrl->new($self->{notebook_1_pane_4}, -1, "joined_DB.fasta", wxDefaultPosition, wxDefaultSize, );
	$self->{panel_16_copy_2} = Wx::Panel->new($self->{notebook_1_pane_4}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_10_copy_8} = Wx::Panel->new($self->{notebook_1_pane_4}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{panel_10_copy_5} = Wx::Panel->new($self->{notebook_1_pane_4}, -1, wxDefaultPosition, wxDefaultSize, );
	$self->{button_7} = Wx::Button->new($self->{notebook_1_pane_4}, -1, "Join files");
	$self->{panel_10_copy_7} = Wx::Panel->new($self->{notebook_1_pane_4}, -1, wxDefaultPosition, wxDefaultSize, );

	$self->__set_properties();
	$self->__do_layout();

# end wxGlade
	return $self;

}


sub __set_properties {
	my $self = shift;

# begin wxGlade: MyFrame::__set_properties

	$self->SetTitle("Fasta Tools");
	$self->{frame_1_statusbar}->SetStatusWidths(-1);
	
	my( @frame_1_statusbar_fields ) = (
		"Ready"
	);

	if( @frame_1_statusbar_fields ) {
		$self->{frame_1_statusbar}->SetStatusText($frame_1_statusbar_fields[$_], $_) 	
		for 0 .. $#frame_1_statusbar_fields ;
	}
	$self->{button_3}->SetMinSize(Wx::Size->new(50, -1));
	$self->{text_ctrl_4}->SetMinSize(Wx::Size->new(250, -1));
	$self->{button_4}->SetMinSize(Wx::Size->new(60, -1));
	$self->{text_ctrl_1}->SetMinSize(Wx::Size->new(400, 150));
	$self->{checkbox_1}->SetValue(1);
	$self->{button_1}->SetBackgroundColour(Wx::Colour->new(143, 143, 188));
	$self->{button_1}->SetFont(Wx::Font->new(8, wxDEFAULT, wxNORMAL, wxBOLD, 0, ""));
	$self->{checkbox_1_copy}->SetValue(1);
	$self->{combo_box_1}->SetSelection(0);
	$self->{notebook_1_pane_1}->SetMinSize(Wx::Size->new(452, 207));
	$self->{combo_box_2}->SetSelection(0);
	$self->{button_2}->SetBackgroundColour(Wx::Colour->new(143, 143, 188));
	$self->{button_2}->SetFont(Wx::Font->new(8, wxDEFAULT, wxNORMAL, wxBOLD, 0, ""));
	$self->{panel_8}->SetMinSize(Wx::Size->new(5,-1));
	$self->{combo_box_3}->SetMinSize(Wx::Size->new(275, 24));
	$self->{combo_box_3}->SetSelection(0);
	$self->{panel_8_copy}->SetMinSize(Wx::Size->new(5,-1));
	$self->{panel_8_copy_copy}->SetMinSize(Wx::Size->new(5,-1));
	$self->{button_5}->SetMinSize(Wx::Size->new(50, -1));
	$self->{panel_8_copy_copy_1}->SetMinSize(Wx::Size->new(5,-1));
	$self->{text_ctrl_6}->SetMinSize(Wx::Size->new(150, -1));
	$self->{button_6}->SetMinSize(Wx::Size->new(90, 23));
	$self->{button_6}->SetBackgroundColour(Wx::Colour->new(143, 143, 188));
	$self->{button_6}->SetFont(Wx::Font->new(8, wxDEFAULT, wxNORMAL, wxBOLD, 0, ""));
	$self->{panel_9}->SetMinSize(Wx::Size->new(20, 5));
	$self->{panel_10}->SetMinSize(Wx::Size->new(393, 5));
	$self->{panel_10_copy}->SetMinSize(Wx::Size->new(20, 5));
	$self->{label_4}->SetFont(Wx::Font->new(8, wxDEFAULT, wxNORMAL, wxBOLD, 0, ""));
	$self->{label_5}->SetFont(Wx::Font->new(10, wxDEFAULT, wxNORMAL, wxBOLD, 0, ""));
	$self->{text_ctrl_7}->SetMinSize(Wx::Size->new(300, -1));
	$self->{text_ctrl_7}->SetFont(Wx::Font->new(10, wxDEFAULT, wxNORMAL, wxBOLD, 0, ""));
	$self->{text_ctrl_7}->Enable(0);
	$self->{label_5_copy}->SetFont(Wx::Font->new(10, wxDEFAULT, wxNORMAL, wxBOLD, 0, ""));
	$self->{text_ctrl_8}->SetMinSize(Wx::Size->new(300, -1));
	$self->{button_8}->SetMinSize(Wx::Size->new(55, -1));
	$self->{label_5_copy_1}->SetFont(Wx::Font->new(10, wxDEFAULT, wxNORMAL, wxBOLD, 0, ""));
	$self->{text_ctrl_9}->SetMinSize(Wx::Size->new(300, -1));
	$self->{button_9}->SetMinSize(Wx::Size->new(55, -1));
	$self->{label_5_copy_1_copy}->SetFont(Wx::Font->new(10, wxDEFAULT, wxNORMAL, wxBOLD, 0, ""));
	$self->{text_ctrl_10}->SetMinSize(Wx::Size->new(300, -1));
	$self->{button_7}->SetBackgroundColour(Wx::Colour->new(143, 143, 188));
	$self->{button_7}->SetFont(Wx::Font->new(8, wxDEFAULT, wxNORMAL, wxBOLD, 0, ""));
	$self->{notebook_1_pane_4}->SetMinSize(Wx::Size->new(452, 207));
	$self->{notebook_1}->SetMinSize(Wx::Size->new(400, 200));
	$self->{notebook_1}->SetFont(Wx::Font->new(10, wxSWISS, wxNORMAL, wxBOLD, 0, ""));

# end wxGlade

#Buttons Management
	EVT_BUTTON( $self, $self->{button_3}, \&general_modules::file_dial_1 );
	EVT_BUTTON( $self, $self->{button_4}, \&general_modules::explore );
	EVT_BUTTON( $self, $self->{button_1}, \&general_modules::analyze1 );
	EVT_BUTTON( $self, $self->{button_2}, \&general_modules::decoy );
	EVT_BUTTON( $self, $self->{button_6}, \&general_modules::search );
	EVT_BUTTON( $self, $self->{button_5}, \&general_modules::file_dial_2 );
	EVT_BUTTON( $self, $self->{button_8}, \&general_modules::file_dial_3 );
	EVT_BUTTON( $self, $self->{button_9}, \&general_modules::file_dial_4 );
	EVT_BUTTON( $self, $self->{button_7}, \&general_modules::join_db );
	
#Eventos barra tareas
	Wx::Event::EVT_MENU($self,1,\&general_modules::exit);
	Wx::Event::EVT_MENU($self,2,\&general_modules::help);
	Wx::Event::EVT_MENU($self,3,\&general_modules::about);


}

sub __do_layout {
	my $self = shift;

# begin wxGlade: MyFrame::__do_layout

	$self->{sizer_1} = Wx::BoxSizer->new(wxVERTICAL);
	$self->{grid_sizer_1} = Wx::FlexGridSizer->new(3, 1, 0, 0);
	$self->{grid_sizer_6} = Wx::FlexGridSizer->new(5, 3, 0, 0);
	$self->{grid_sizer_8} = Wx::FlexGridSizer->new(1, 3, 4, 0);
	$self->{grid_sizer_7} = Wx::FlexGridSizer->new(4, 3, 2, 2);
	$self->{grid_sizer_5} = Wx::FlexGridSizer->new(6, 6, 3, 0);
	$self->{grid_sizer_4} = Wx::FlexGridSizer->new(5, 6, 11, 0);
	$self->{grid_sizer_3} = Wx::FlexGridSizer->new(11, 6, 0, 0);
	$self->{grid_sizer_2} = Wx::FlexGridSizer->new(1, 6, 3, 1);
	$self->{grid_sizer_2}->Add($self->{panel_1}, 1, wxEXPAND, 0);
	$self->{grid_sizer_2}->Add($self->{button_3}, 0, 0, 0);
	$self->{grid_sizer_2}->Add($self->{text_ctrl_4}, 0, 0, 0);
	$self->{grid_sizer_2}->Add($self->{panel_3}, 1, wxEXPAND, 0);
	$self->{grid_sizer_2}->Add($self->{button_4}, 0, 0, 0);
	$self->{grid_sizer_2}->Add($self->{panel_4}, 1, wxEXPAND, 0);
	$self->{grid_sizer_2}->AddGrowableCol(5);
	$self->{grid_sizer_1}->Add($self->{grid_sizer_2}, 1, wxEXPAND, 0);
	$self->{grid_sizer_1}->Add($self->{text_ctrl_1}, 0, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_3_copy}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_3_copy_1}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_3_copy_2}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_3_copy_3}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_1}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{checkbox_1}, 0, wxALIGN_CENTER_HORIZONTAL, 0);
	$self->{grid_sizer_3}->Add($self->{label_1}, 0, 0, 0);
	$self->{grid_sizer_3}->Add($self->{button_1}, 0, wxALIGN_RIGHT, 0);
	$self->{grid_sizer_3}->Add($self->{panel_3_copy_2_copy}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_3_copy_7}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_2}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{checkbox_1_copy}, 0, wxALIGN_CENTER_HORIZONTAL, 0);
	$self->{grid_sizer_3}->Add($self->{label_1_copy}, 0, 0, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_10}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_15}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_3_copy_11}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_3}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{checkbox_1_copy_1}, 0, wxALIGN_CENTER_HORIZONTAL, 0);
	$self->{grid_sizer_3}->Add($self->{label_1_copy_1}, 0, 0, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_11}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_12}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_13}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_3_copy}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_3_copy_1}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{label_1_copy_1_copy}, 0, 0, 0);
	$self->{grid_sizer_3}->Add($self->{combo_box_1}, 0, 0, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_3_copy_1_copy_2}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_3_copy_1_copy_3}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_3_copy_1_copy}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_3_copy_1_copy_1}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{label_1_copy_1_copy_copy}, 0, 0, 0);
	$self->{grid_sizer_3}->Add($self->{text_ctrl_3}, 0, 0, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_3_copy_1_copy_4}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_3_copy_1_copy_5}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_3_copy_1_copy_4_copy_4}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_3_copy_1_copy_4_copy_5}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{label_1_copy_1_copy_copy_1}, 0, 0, 0);
	$self->{grid_sizer_3}->Add($self->{text_ctrl_3_copy}, 0, 0, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_3_copy_1_copy_4_copy}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_3_copy_1_copy_4_copy_2}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_3_copy_1_copy_4_copy_6}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_3_copy_1_copy_4_copy_7}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{label_1_copy_1_copy_copy_1_copy}, 0, 0, 0);
	$self->{grid_sizer_3}->Add($self->{text_ctrl_3_copy_1}, 0, 0, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_3_copy_1_copy_4_copy_1}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_3_copy_1_copy_4_copy_3}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_3_copy_1_copy_4_copy_6_copy}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_3_copy_1_copy_4_copy_6_copy_1}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_3_copy_1_copy_4_copy_6_copy_2}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_3_copy_1_copy_4_copy_6_copy_3}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_3_copy_1_copy_4_copy_6_copy_4}, 1, wxEXPAND, 0);
	$self->{grid_sizer_3}->Add($self->{panel_2_copy_3_copy_1_copy_4_copy_6_copy_5}, 1, wxEXPAND, 0);
	$self->{notebook_1_pane_1}->SetSizer($self->{grid_sizer_3});
	$self->{grid_sizer_3}->AddGrowableRow(10);
	$self->{grid_sizer_4}->Add($self->{panel_5}, 1, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{panel_5_copy}, 1, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{panel_5_copy_1}, 1, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{panel_5_copy_2}, 1, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{panel_5_copy_3}, 1, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{panel_5_copy_4}, 1, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{panel_5_copy_5}, 1, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{label_2}, 0, wxALIGN_RIGHT, 0);
	$self->{grid_sizer_4}->Add($self->{combo_box_2}, 0, 0, 0);
	$self->{grid_sizer_4}->Add($self->{panel_5_copy_6}, 1, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{panel_5_copy_6_copy_4}, 1, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{panel_5_copy_7}, 1, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{panel_5_copy_8}, 1, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{panel_6}, 1, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{button_2}, 0, wxALIGN_CENTER_HORIZONTAL, 0);
	$self->{grid_sizer_4}->Add($self->{panel_5_copy_6_copy}, 1, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{panel_5_copy_6_copy_1}, 1, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{panel_5_copy_6_copy_2}, 1, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{panel_5_copy_6_copy_3}, 1, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{label_2_copy_1}, 0, wxALIGN_RIGHT, 0);
	$self->{grid_sizer_4}->Add($self->{text_ctrl_2}, 0, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{panel_5_copy_6_copy_3_copy}, 1, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{panel_5_copy_6_copy_3_copy_1}, 1, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{panel_5_copy_6_copy_3_copy_2}, 1, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{panel_5_copy_6_copy_3_copy_3}, 1, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{panel_5_copy_6_copy_3_copy_3_copy}, 1, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{checkbox_2}, 0, 0, 0);
	$self->{grid_sizer_4}->Add($self->{panel_5_copy_6_copy_3_copy_6}, 1, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{panel_5_copy_6_copy_3_copy_7}, 1, wxEXPAND, 0);
	$self->{grid_sizer_4}->Add($self->{panel_5_copy_6_copy_3_copy_8}, 1, wxEXPAND, 0);
	$self->{notebook_1_pane_2}->SetSizer($self->{grid_sizer_4});
	$self->{grid_sizer_5}->Add($self->{panel_7}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{panel_7_copy}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{panel_7_copy_1}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{panel_8}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{panel_7_copy_3}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{panel_7_copy_4}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{panel_7_copy_5}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{label_3}, 0, wxALIGN_RIGHT, 0);
	$self->{grid_sizer_5}->Add($self->{combo_box_3}, 0, 0, 0);
	$self->{grid_sizer_5}->Add($self->{panel_8_copy}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{panel_8_copy_copy}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{panel_7_copy_8}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{panel_7_copy_9}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{label_3_copy}, 0, wxALIGN_RIGHT, 0);
	$self->{grid_sizer_5}->Add($self->{text_ctrl_5}, 0, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{button_5}, 0, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{panel_8_copy_copy_1}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{panel_7_copy_11}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{panel_7_copy_12}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{panel_7_copy_12_copy}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{checkbox_3}, 0, 0, 0);
	$self->{grid_sizer_5}->Add($self->{panel_7_copy_12_copy_copy}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{panel_7_copy_14}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{panel_7_copy_15}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{panel_7_copy_16}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{label_3_copy_copy}, 0, 0, 0);
	$self->{grid_sizer_5}->Add($self->{text_ctrl_6}, 0, 0, 0);
	$self->{grid_sizer_5}->Add($self->{panel_7_copy_12_copy_copy_copy}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{panel_7_copy_18}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{panel_7_copy_19}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{panel_7_copy_20}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{panel_7_copy_21}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{button_6}, 0, wxALIGN_CENTER_HORIZONTAL, 0);
	$self->{grid_sizer_5}->Add($self->{panel_7_copy_12_copy_copy_copy_1}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{panel_7_copy_24}, 1, wxEXPAND, 0);
	$self->{grid_sizer_5}->Add($self->{panel_7_copy_25}, 1, wxEXPAND, 0);
	$self->{notebook_1_pane_3}->SetSizer($self->{grid_sizer_5});
	$self->{grid_sizer_6}->Add($self->{panel_9}, 1, wxEXPAND, 0);
	$self->{grid_sizer_6}->Add($self->{panel_10}, 1, wxEXPAND, 0);
	$self->{grid_sizer_6}->Add($self->{panel_10_copy}, 1, wxEXPAND, 0);
	$self->{grid_sizer_6}->Add($self->{panel_10_copy_1}, 1, wxEXPAND, 0);
	$self->{grid_sizer_6}->Add($self->{label_4}, 0, wxALIGN_CENTER_HORIZONTAL, 0);
	$self->{grid_sizer_6}->Add($self->{panel_10_copy_2}, 1, wxEXPAND, 0);
	$self->{grid_sizer_6}->Add($self->{panel_10_copy_3}, 1, wxEXPAND, 0);
	$self->{grid_sizer_7}->Add($self->{label_5}, 0, 0, 0);
	$self->{grid_sizer_7}->Add($self->{text_ctrl_7}, 0, wxALIGN_RIGHT, 0);
	$self->{grid_sizer_7}->Add($self->{panel_11}, 1, wxEXPAND, 0);
	$self->{grid_sizer_7}->Add($self->{label_5_copy}, 0, 0, 0);
	$self->{grid_sizer_7}->Add($self->{text_ctrl_8}, 0, 0, 0);
	$self->{grid_sizer_7}->Add($self->{button_8}, 0, 0, 0);
	$self->{grid_sizer_7}->Add($self->{label_5_copy_1}, 0, 0, 0);
	$self->{grid_sizer_7}->Add($self->{text_ctrl_9}, 0, 0, 0);
	$self->{grid_sizer_7}->Add($self->{button_9}, 0, 0, 0);
	$self->{grid_sizer_7}->Add($self->{panel_16}, 1, wxEXPAND, 0);
	$self->{grid_sizer_7}->Add($self->{panel_16_copy}, 1, wxEXPAND, 0);
	$self->{grid_sizer_7}->Add($self->{panel_16_copy_1}, 1, wxEXPAND, 0);
	$self->{grid_sizer_6}->Add($self->{grid_sizer_7}, 1, wxEXPAND|wxALIGN_CENTER_HORIZONTAL, 0);
	$self->{grid_sizer_6}->Add($self->{panel_10_copy_9}, 1, wxEXPAND, 0);
	$self->{grid_sizer_6}->Add($self->{panel_10_copy_4}, 1, wxEXPAND, 0);
	$self->{grid_sizer_8}->Add($self->{label_5_copy_1_copy}, 0, 0, 0);
	$self->{grid_sizer_8}->Add($self->{text_ctrl_10}, 0, 0, 0);
	$self->{grid_sizer_8}->Add($self->{panel_16_copy_2}, 1, wxEXPAND, 0);
	$self->{grid_sizer_6}->Add($self->{grid_sizer_8}, 1, wxEXPAND, 0);
	$self->{grid_sizer_6}->Add($self->{panel_10_copy_8}, 1, wxEXPAND, 0);
	$self->{grid_sizer_6}->Add($self->{panel_10_copy_5}, 1, wxEXPAND, 0);
	$self->{grid_sizer_6}->Add($self->{button_7}, 0, wxALIGN_CENTER_HORIZONTAL, 0);
	$self->{grid_sizer_6}->Add($self->{panel_10_copy_7}, 1, wxEXPAND, 0);
	$self->{notebook_1_pane_4}->SetSizer($self->{grid_sizer_6});
	$self->{notebook_1}->AddPage($self->{notebook_1_pane_1}, "Analyze");
	$self->{notebook_1}->AddPage($self->{notebook_1_pane_2}, "Decoy");
	$self->{notebook_1}->AddPage($self->{notebook_1_pane_3}, "Search");
	$self->{notebook_1}->AddPage($self->{notebook_1_pane_4}, "Join");
	$self->{grid_sizer_1}->Add($self->{notebook_1}, 1, wxEXPAND, 0);
	$self->{sizer_1}->Add($self->{grid_sizer_1}, 1, wxEXPAND, 0);
	$self->SetSizer($self->{sizer_1});
	$self->{sizer_1}->Fit($self);
	$self->Layout();

# end wxGlade
}

# end of class MyFrame





1;

package fastatools;

use base qw(Wx::App);
use strict;

sub OnInit {
	my( $self ) = shift;

	Wx::InitAllImageHandlers();

	my $frame_1 = MyFrame->new();

	$self->SetTopWindow($frame_1);
	$frame_1->Show(1);

	return 1;
}
# end of class fastatools

package main;

#unless(caller){
	my $fastaTools = fastatools->new();
	$fastaTools->MainLoop();
#}


package general_modules;

use strict;
use File::Copy;
use Wx;
use Wx qw[:everything];
use base qw(Wx::Frame);
use Wx::Event qw(EVT_BUTTON);


#############		GLOBAL VARS	ANALYZE	###############################################
my %frags_tripticos;
my $num_frags_trypt=0;
my $num_frags_trypt_redundant=0;
my $num_frags_trypt_out_mass_range=0;

my %masses= (
	A => 71.03711,R => 156.10111,N => 114.04293,D => 115.02694,C => 103.00919,
	E => 129.04259,Q => 128.05858,G => 57.02146,H => 137.05891,I => 113.08406,
	L => 113.08406,K => 128.09496,M => 131.04049,F => 147.06841,P => 97.05276,
	S => 87.03203,T => 101.04768,W => 186.07931,Y => 163.06333,V => 99.06841,  
		);			
####################################################################################



sub about {
  my $this = shift;
  Wx::MessageBox( "   FASTA TOOLS v0.9      \n        David Ovelleiro      \n  dovelleiro\@gmail.com      \n" .
                  "        CSIC-UAB\n" . "",
                  "About FASTA TOOLS", wxOK|wxCENTRE|wxICON_INFORMATION, $this );
    
	
	
	                               
}


sub exit{
	my( $this, $event ) = @_;
	$this->Close( 1 );
	}

sub help{
	#my $this = shift;
  	#my $answer=Wx::MessageBox( "To obtain documentation of this Software you'll be redirected to the Project Main Page\n" .
                  #"Press 'Yes' if you want this page is loaded on you default Web Browser\n" . "",
                  #"FASTA TOOLS HELP", wxYES_NO|wxCENTRE|wxICON_QUESTION , $this );
	
	#if($answer==wxYES){
		##my $url = 'http://www.lymphos.org';
		##system("explorer $url");
		#use Win32::FileOp qw(ShellExecute);
		#ShellExecute("http://fastatools.sourceforge.net/docs.html");
	#}		
	
}

sub file_dial_1{
	my( $this, $event ) = @_;
    my $dialog = Wx::FileDialog->new
      ( $this, "Select a Fasta file", '', '',"Fasta (*.fasta)|*.fasta|All files (*.*)|*.*",0);
    if( $dialog->ShowModal == wxID_OK ) {
      my @paths = $dialog->GetPaths;
      if( @paths == 1 ) {
        foreach ( @paths ) {
        	$this->{text_ctrl_4}->SetValue($paths[0]);
        	$this->{text_ctrl_7}->SetValue($paths[0]);
        }
      } 
    }
    $dialog->Destroy;
}

sub file_dial_2{
	my( $this, $event ) = @_;
    my $dialog = Wx::FileDialog->new
      ( $this, "Select a list file", '', '',"Text (*.txt)|*.txt|All files (*.*)|*.*",0);
    if( $dialog->ShowModal == wxID_OK ) {
      my @paths = $dialog->GetPaths;
      if( @paths == 1 ) {
        foreach ( @paths ) {
        	$this->{text_ctrl_5}->SetValue($paths[0]);
        }
      } 
    }
    $dialog->Destroy;
}

sub file_dial_3{
	my( $this, $event ) = @_;
    my $dialog = Wx::FileDialog->new
      ( $this, "Select a Fasta file", '', '',"Fasta (*.fasta)|*.fasta|All files (*.*)|*.*",0);
    if( $dialog->ShowModal == wxID_OK ) {
      my @paths = $dialog->GetPaths;
      if( @paths == 1 ) {
        foreach ( @paths ) {
        	$this->{text_ctrl_8}->SetValue($paths[0]);
        }
      } 
    }
    $dialog->Destroy;
}
sub file_dial_4{
	my( $this, $event ) = @_;
    my $dialog = Wx::FileDialog->new
      ( $this, "Select a Fasta file", '', '',"Fasta (*.fasta)|*.fasta|All files (*.*)|*.*",0);
    if( $dialog->ShowModal == wxID_OK ) {
      my @paths = $dialog->GetPaths;
      if( @paths == 1 ) {
        foreach ( @paths ) {
        	$this->{text_ctrl_9}->SetValue($paths[0]);
        }
      } 
    }
    $dialog->Destroy;
}
sub join_db{
my( $this, $event ) = @_;
	my $fasta_A= $this->{text_ctrl_4}->GetValue;
	my $fasta_B= $this->{text_ctrl_8}->GetValue;
	my $fasta_C= $this->{text_ctrl_9}->GetValue;
	my $fasta_D= $this->{text_ctrl_10}->GetValue;
	
	$this->{text_ctrl_1}->Clear;
	
	#Tests for File A
		unless (-T $fasta_A){
				$this->{text_ctrl_1}->AppendText("Please, select a Fasta protein database using the ". 
				"\'Browse\' \n button in the left upper corner of the application.");
				return;
			}
	
	#Tests for file B and C
	unless(-T $fasta_B){
			$fasta_B="";
	}
	unless(-T $fasta_C){
			$fasta_C="";
	}
	if(!$fasta_B and !$fasta_C){
		$this->{text_ctrl_1}->AppendText("Please, select at least one Fasta protein\n database using the ". 
				"\'Browse\' button in the fields B or C\nThese will be the files to be joined to the\n main database.");
				return;
	}
	
	#Tests for file D
		my $out_dir=dir_from_file($fasta_A);
		unless($out_dir){
			$this->{text_ctrl_1}->Clear;
			$this->{text_ctrl_1}->AppendText("Error while creating the out file (D file)"."\n");
			return;
		}
		$fasta_D=$out_dir.$fasta_D;
		if(-f$fasta_D){#File already exists
			my $answer=Wx::MessageBox( "The out file you try to create to store the decoy database" .
						  "already exists. \n".
						  "Do you want to overwrite it?\n" . "",
						  "FastaTools Warning!!!", wxYES_NO|wxCENTRE|wxICON_QUESTION , $this );
			if($answer==wxYES){
				unlink $fasta_D;
				$this->Refresh();
				$this->Update();		
			}
			else{
				$this->{text_ctrl_1}->Clear;
				$this->{text_ctrl_1}->AppendText("Please, select another name in the \'D\' field"."\n");
				$this->Refresh();
				$this->Update();
				return;	
			}
		
		}
	
	#All tests correct: the main Database and at least one (B or C) are test files
	$this->{text_ctrl_1}->AppendText("File to be created: \n $fasta_D"."\n");
	$this->{text_ctrl_1}->AppendText("Files to be joined:"."\n");
	$this->{frame_1_statusbar}->SetStatusText('Copying files');
	
		my $t0=time();
		if(open FASTA,">>$fasta_D" ){
		$this->{text_ctrl_1}->AppendText($fasta_A."\n");
		$this->Refresh();
		$this->Update();
			if(open FASTAA,"$fasta_A"){
				while(<FASTAA>){
					print FASTA;
				}
				close FASTAA;
			}
			else{
				$this->{text_ctrl_1}->Clear;
				$this->{text_ctrl_1}->AppendText("Error while reading the main file (A file)"."\n");
				return;
			}
			if($fasta_B){
				$this->{text_ctrl_1}->AppendText($fasta_B."\n");
				open FASTAB,"<$fasta_B" or return;
				while(<FASTAB>){
					print FASTA;
				}
				close FASTAB;
			}
			if($fasta_C){
				$this->Refresh();
				$this->Update();
				$this->{text_ctrl_1}->AppendText($fasta_C."\n");
				open FASTAC,"<$fasta_C";
				while(<FASTAC>){
					print FASTA;
				}
				close FASTAC;
			}
			close FASTA;
		}

	my $tf=time();
	my $t= $tf-$t0;
	$this->{text_ctrl_1}->AppendText("Time: $t secs"."\n");
	$this->{text_ctrl_1}->AppendText("Files have been joined"."\n");
	$this->{frame_1_statusbar}->SetStatusText('Ready');
	
}
sub explore{
	my( $this, $event ) = @_;
	my $fasta_file= $this->{text_ctrl_4}->GetValue;
	$this->{text_ctrl_1}->Clear;
	if(-T$fasta_file){
		open FASTA, "<$fasta_file";
		my $i=0;
		while(<FASTA>){
			if($i>200){
				last;
				}
			$i++;
			$this->{text_ctrl_1}->AppendText("$_");	
		}
		close FASTA;
	$this->{text_ctrl_1}->AppendText("\n ---  More  ---\n");			
	}
	else{
		$this->{text_ctrl_1}->AppendText("The selected file   $fasta_file   is not a text file\n");	
		}
}

#################################################################################################
##########################		Analyze functionality		#####################################



sub analyze{
	my( $this, $event ) = @_;#@_=();
	my $t0=time();
	
	undef %frags_tripticos;
	$num_frags_trypt=0;
	$num_frags_trypt_redundant=0;
	$num_frags_trypt_out_mass_range=0;


	#Arguments management
	my $file=$this->{text_ctrl_4}->GetValue;
	my $number_prots=$this->{checkbox_1}->GetValue;
	my $aas_frecs=$this->{checkbox_1_copy}->GetValue;
	my $number_fragments=$this->{checkbox_1_copy_1}->GetValue;
	my $enzyme=$this->{combo_box_1}->GetValue; 
	my $missed_cleavages=$this->{text_ctrl_3}->GetValue;
	my $min_mass=$this->{text_ctrl_3_copy}->GetValue;
	my $max_mass=$this->{text_ctrl_3_copy_1}->GetValue;
	$this->{text_ctrl_1}->Clear;

	

	
	#What happens if no Fasta file is selected
	unless (-T $file){
			$this->{text_ctrl_1}->Clear;
			$this->{text_ctrl_1}->AppendText("Please, select a Fasta protein database using the ". 
			"\'Browse\'\nbutton in the left upper corner of the application.");
			return;
		}
	#Fasta lines prediction
	$this->{frame_1_statusbar}->SetStatusText('Progress: Loading the file.');
	my $lines_predicted= test_file_Fasta($file);
	unless ($lines_predicted){
		$this->{text_ctrl_1}->Clear;
		$this->{text_ctrl_1}->AppendText("Incorrect Fasta file\n");
		return;
	}	

	#What happens if none of the checkboxes is selected
	if(($number_prots eq '')and($aas_frecs eq '')and($number_fragments eq '')){
		$this->{text_ctrl_1}->Clear;
		$this->{text_ctrl_1}->AppendText("Please, select at least one analysis option \n- Number of proteins,\n- Amino acid frequencies \n- Number of fragments\n");
		return;
	}
	$this->{frame_1_statusbar}->SetStatusText('Beginning analysis');

	#Test if the file is too big for fragment calculation
	if($number_fragments){
		my $filesize = -s $file;
		if($filesize>60000000){
			#Warning: The file size is too big to perform this operation
			my $answer=Wx::MessageBox( "The file you have selected is too big to perform the \'Number of Fragments\' " .
					  "analysis. If you proceed, it will take a long time and probably your computer will run out of memory\n".
					  "Do you want to proceed anyway?\n" . "",
					  "FastaTools Warning!!!", wxYES_NO|wxCENTRE|wxICON_QUESTION , $this );
			unless($answer==wxYES){
				$this->{text_ctrl_1}->Clear;
				$this->Refresh();
				$this->Update();
				return;
			}
		}
	}
	$this->{text_ctrl_1}->Clear;


	#Call main soubrutine
	my ($tr1)= threads->create(\&analyze_file,$this,$lines_predicted,$file,$number_prots,$aas_frecs,$number_fragments,$enzyme,$min_mass,$max_mass,$missed_cleavages);
	my @ret=$tr1->join();
	
	my ($num_proteins,$aas)=@ret; 
	
	
	#########	Reporting obtained data
	if($number_prots){
		$this->{text_ctrl_1}->AppendText("Number of proteins in database: ".format_commas($num_proteins)."\n");
		$this->{text_ctrl_1}->AppendText("---------------------------------------\n");
	}
	if($aas_frecs){
		$this->{text_ctrl_1}->AppendText("Amino acid composition in database:\n");
		my %freqs=%{$aas};
		my $line_count=0;
		foreach(keys %freqs){
			$line_count++;
			my $rounded_freq=sprintf ("%.4f",$freqs{$_});
			$this->{text_ctrl_1}->AppendText("$_\t$rounded_freq%\t\t");
			if($line_count>1){
				$line_count=0;
				$this->{text_ctrl_1}->AppendText("\n");
			}	
		}
	$this->{text_ctrl_1}->AppendText("\n---------------------------------------\n");
	}
	if($number_fragments){
		$this->{text_ctrl_1}->AppendText("Enzyme cleaved fragments:\n");
		$this->{text_ctrl_1}->AppendText("Non redundant fragments\t".format_commas($num_frags_trypt)."\n");
		$this->{text_ctrl_1}->AppendText("Redundant fragments\t".format_commas($num_frags_trypt_redundant)."\n");
		$this->{text_ctrl_1}->AppendText("Out of range fragments\t".format_commas($num_frags_trypt_out_mass_range)."\n");
		$this->{text_ctrl_1}->AppendText("---------------------------------------\n");
	}	
	%frags_tripticos=();
	my $tf=time();
	my $t=$tf-$t0;
	$this->{text_ctrl_1}->AppendText("Total time: $t sec\n");
	$this->{frame_1_statusbar}->SetStatusText('Ready');
	return;	
}


sub analyze_file{
	my ($this,$lines_predicted,$file,$num_proteins,$aa_freq,$num_frags,$enzyme,$min_mass,$max_mass,$missed_cleavages)=@_;
	
	my $enzyme_regex;
	if($enzyme eq 'Trypsin (KR|P)'){
		$enzyme_regex='(?<=[KR])(?=[^P])';
		}
	elsif($enzyme eq 'Trypsin (KR)'){
		$enzyme_regex='(?<=[KR])';
		}
	elsif($enzyme eq 'GluC (DE)'){
		$enzyme_regex='(?<=[DE])';
		}
	elsif($enzyme eq 'GluC (E)'){
		$enzyme_regex='(?<=[E])';
		}
	elsif($enzyme eq 'Endolysin (K)'){
		$enzyme_regex='(?<=[K])';
		}
	else{
		$enzyme_regex='';
	}	
	
	my %aas= (
	A => 0,R => 0,N => 0,D => 0,C => 0,	E => 0,Q => 0,G => 0,H => 0,I => 0,
	L => 0,K => 0,M => 0,F => 0,P => 0,	S => 0,T => 0,W => 0,Y => 0,V => 0,  
		);
	##########	Create progress milestones	
	my @ten_line_numbers;
	push @ten_line_numbers,0;
	for(my $i=1;$i<101;$i++){
		push @ten_line_numbers,(($lines_predicted/100)*$i);
	}
	##########	Open the file to analyze
	open FASTA,"<$file";
	while(<FASTA>){
		if($_=~/^\>/){
			last;
		}
	}
	my $line=0;
	my $sec="";#
	my $prot=1;#for prot number
	my $num_aa=0;#for aa freqs
	my $progress_num=1;
	while(<FASTA>){
		$line++;
		if ($_=~/^\>/){
			$prot++;
			#### Progress
			if ($progress_num<99){
				if($line>$ten_line_numbers[$progress_num]){
					$progress_num++;
					$this->{frame_1_statusbar}->SetStatusText('Progress: '.$progress_num.' %');
					$this->Refresh();
					$this->Update();
				}
			}
			elsif($progress_num==99){
				$progress_num++;
				$this->{frame_1_statusbar}->SetStatusText('Progress: 99%');
			}
			
			#Calculation of number of missed cleavages
			
			if($enzyme_regex and $num_frags){
				cleavage($sec,$missed_cleavages,$min_mass,$max_mass,$enzyme_regex);
			}
			#Calculation of amino acid frequences (%aas)
			if($aa_freq){
				my @as=split //,$sec;
				foreach my $a(@as){
				$num_aa++;
					if(defined $aas{$a}){
						$aas{$a}=$aas{$a}+1;
					}
					else{
						$aas{$a}=0;
						$aas{$a}=$aas{$a}+1;
					}
				}
			}
			
			#temporary sequence empty
			$sec="";
		}
		else{
			$_=~s/\W//g;
			$sec.=$_;
		}
	}
##########	LAST SEQUENCE PROCESS
	#Calculation of number of missed cleavages
			if($enzyme_regex and $num_frags){
				cleavage($sec,$missed_cleavages,$min_mass,$max_mass,$enzyme_regex);
			}
			#Calculation of amino acid frequences (%aas)
			if($aa_freq){
				my @as=split //,$sec;
				foreach my $a(@as){
				$num_aa++;
					if(defined $aas{$a}){
						$aas{$a}=$aas{$a}+1;
					}
					else{
						$aas{$a}=0;
						$aas{$a}=$aas{$a}+1;
					}
				}
			}
	$sec="";			
##########	END OF LAST SEQUENCE PROCESS
	
	if($aa_freq){
		foreach(keys %aas){
			$aas{$_}=100*($aas{$_}/$num_aa);
		}
	}
	
	return ($prot,\%aas);
	
}



sub cleavage{
	#	This subroutine produces the cleavage over a sequence. 
	#	It accepts 5 arguments:
	#		-$sec-> sequence of a protein, without carriage returns
	#		-$num_missed-> number of missed cleavages
	#		-$min_mass-> minimum mass for a peptide to be accepted
	#		-$max_mass-> maximum  mass for a peptide to be accepted
	#		-$cleavage-> cleavage Regular Expression
	#	To increase the script speed, there is no output. The global variables
	#	%frags_tripticos, $num_frags_trypt, $num_frags_trypt_out_mass_range and
	#	$num_frags_trypt_redundant are filled or increased. 
	
	my($sec,$num_missed,$min_mass,$max_mass,$cleavage)=@_;
	my @peptidos= split(qr/$cleavage/, $sec);
	my @peptidos_in_range;
	#missed cleavages
	my ($numPept, $missed);
  	$numPept = @peptidos;
  	for (my $i = 0; $i < $numPept-1; $i++){
		$missed = $peptidos[$i];
		for (my $j = 1; ($j <= $num_missed) && ($i+$j < $numPept); $j++){
		  $missed .= $peptidos[$i+$j];
		  push(@peptidos, $missed);
		 }
	}
	
	foreach(@peptidos){
		if(defined $frags_tripticos{$_}){
			$num_frags_trypt_redundant++;	
		}
		else{
			my $pep_mass=&mass_peptide($_);
			unless (($pep_mass>$max_mass)||($pep_mass<$min_mass)){
				$frags_tripticos{$_}=1;
				$num_frags_trypt++;	
			}
			else{
				$num_frags_trypt_out_mass_range++;
			}
		}
	}
}


##########################		End of Analyze functionality		#############################
#################################################################################################
#################################################################################################

#################################################################################################
#################################################################################################
##########################		Decoy functionality					#############################
sub decoy{
	use File::Copy;
######	Arguments management
	my( $this, $event ) = @_;
	$this->{text_ctrl_1}->Clear;
	my $decoy=$this->{combo_box_2}->GetValue;
	#out_file1:main fasta, out_file2: decoy fasta to generate
	my ($out_dir,$out_file1,$out_file2);
	my $append_original_file=$this->{checkbox_2}->GetValue;
	$out_file1= $this->{text_ctrl_4}->GetValue;
	$out_file2= $this->{text_ctrl_2}->GetValue;
	my $out_file2_short= $out_file2;
	#What happens if no Fasta file is selected
	unless (-T $out_file1){
		$this->{text_ctrl_1}->Clear;
		$this->{text_ctrl_1}->AppendText("Please, select a Fasta protein database using the ". 
			"\'Browse\'\nbutton in the left upper corner of the application.");
		return;
		}
	
	
	#Generation of the complete paths
	$out_dir=dir_from_file($out_file1);
	unless($out_dir){
		$this->{text_ctrl_1}->Clear;
		$this->{text_ctrl_1}->AppendText("Error while creating the out file"."\n");
		return;
	}
	$out_file2=$out_dir.$out_file2;
	#Test if the out_file2 already exists
	if(-f $out_file2){
		my $answer=Wx::MessageBox( "The out file you try to create to store the decoy database" .
					  "already exists. \n".
					  "Do you want to overwrite it?\n" . "",
					  "FastaTools Warning!!!", wxYES_NO|wxCENTRE|wxICON_QUESTION , $this );
		if($answer==wxYES){
			unlink $out_file2;
			$this->Refresh();
			$this->Update();		
		}
		else{
			$this->{text_ctrl_1}->Clear;
			$this->{text_ctrl_1}->AppendText("Please, select another name in the \'Out name\' field"."\n");
			$this->Refresh();
			$this->Update();
			return;	
		}	
	}
	#Fasta file Test to main fasta
	$this->{frame_1_statusbar}->SetStatusText('Progress: Loading the file.');
	my $lines_predicted= test_file_Fasta($out_file1);
	unless($lines_predicted){
		$this->{text_ctrl_1}->Clear;
		$this->{text_ctrl_1}->AppendText("Incorrect Fasta file\n");
		return;
	}
	else{
		open FASTA,"$out_file1";
	}
	#Generation of the physical file anf filehandle if 'append' option, or only opening the filehanle if not	
	if ($append_original_file){#if append
		unless(copy( $out_file1, $out_file2 )){
			$this->{text_ctrl_1}->Clear;
			$this->{text_ctrl_1}->AppendText($out_file1."\n");
			$this->{text_ctrl_1}->AppendText("Unable to create the out file.\nPlease, check the \'Out file\' name."."\n");
			return;
		}
		else{
			open FILE_IN,">>$out_file2";
			}
	}
	else{#if not append
		unless(open FILE_IN,">$out_file2"){
			$this->{text_ctrl_1}->Clear;
			$this->{text_ctrl_1}->AppendText($out_file2."\n");
			$this->{text_ctrl_1}->AppendText("Unable to create the out file.\nPlease, check the \'Out file\' name."."\n");
			return;
		}
	}				
	#Treatment of the process arguments
	my $treatment;
	my $enzyme_regex='';
	if($decoy eq 'Reverse'){
		$enzyme_regex='Reverse';
		$treatment=\&reverse_seq;
	}
	elsif($decoy eq 'Random'){
		$enzyme_regex='Random';
		$treatment=\&alea_seq;
	}
	elsif($decoy eq 'PseudoReverse Trypsin (KR|P)'){
		$enzyme_regex='Pseudo trups KR|P';
		$treatment=\&pseudoreverse_trypsinKRP;
	}
	else{
		if($decoy=~/^PseudoReverse/){
			$treatment=\&pseudoreverse;
				if($decoy eq 'PseudoReverse Trypsin (KR)'){$enzyme_regex='(?<=[KR])';}
				elsif($decoy eq 'PseudoReverse GluC (DE)'){$enzyme_regex='(?<=[DE])';}
				elsif($decoy eq 'PseudoReverse GluC (E)'){$enzyme_regex='(?<=[E])';}
				elsif($decoy eq 'PseudoReverse Endolysin (K)'){$enzyme_regex='(?<=[K])';}	
		}
	}	
######	Ends argument management
		
	$this->{text_ctrl_1}->Clear;
	$this->{text_ctrl_1}->AppendText("Starting Decoy DB generation: $decoy\n");	
	
	
	##########	Create progress milestones	
	my @ten_line_numbers;
	for(my $i=1;$i<101;$i++){
		push @ten_line_numbers,(($lines_predicted/100)*$i);
	}
	##########
	
	while(<FASTA>){
		if($_=~/^\>/){
			last;
		}
	}
	
	my $line=0;	
	my $sec="";#
	my $prot=1;#for prot number
	my $progress_num=1;
	my $length_seq;
	while(<FASTA>){
		$line++;
		if ($_=~/^\>/){
			print FILE_IN '>decoy_'."$prot\n";
			$prot++;
			#### Progress
			if ($progress_num<99){
				if($line>$ten_line_numbers[$progress_num]){
					$progress_num++;
					$this->{frame_1_statusbar}->SetStatusText('Progress: '.$progress_num.' %');
					$this->Refresh();
					$this->Update();
				}
			}
			elsif($progress_num==99){
				$progress_num++;
				$this->{frame_1_statusbar}->SetStatusText('Progress: Writting to disk.');
			}

			#Reverse sequence
			my $decoy_sequence= &{$treatment}($sec,$enzyme_regex);
			#Format sequence in 60 letters/line
			$length_seq=(length($decoy_sequence))/60;
			for (my $k=0;$k<$length_seq;$k++){
				print FILE_IN (substr $decoy_sequence,$k*60,60);
				print FILE_IN "\n";
			}  
			
			#temporary sequence empty
			$sec="";
		}
		else{
			$_=~s/\W//g;
			$sec.=$_;
		}
	}
##########	LAST SEQUENCE PROCESS
	#Reverse sequence
	print FILE_IN '>decoy_'."$prot\n";
	my $decoy_sequence= &{$treatment}($sec,$enzyme_regex);
	#Format sequence in 60 letters/line
	$length_seq=(length($decoy_sequence))/60;
	for (my $k=0;$k<$length_seq;$k++){
		print FILE_IN (substr $decoy_sequence,$k*60,60);
		print FILE_IN "\n";
	}		
	$sec="";			
##########	END OF LAST SEQUENCE PROCESS
	
	close FASTA;		
	close FILE_IN;
	#Success notification
	$this->{frame_1_statusbar}->SetStatusText('Ready');
	$this->{text_ctrl_1}->Clear;
	$this->{text_ctrl_1}->AppendText("The decoy database \'$out_file2_short\' has been \nsuccessfully generated"."\n");
	$this->Refresh();
	$this->Update();
		
	return;
	

}

sub reverse_seq{
	my $secuencia= $_[0];
	$secuencia=reverse $secuencia;
	return $secuencia;
	}
sub alea_seq{
	my $secuencia= $_[0];
    my @trozos= split //, $secuencia;
    my $secuencia_rand=""; 
    
	my @TempList = ();
	while(@trozos){
		push(@TempList, splice(@trozos, rand(@trozos), 1)) 
	}
	@trozos = @TempList;
	for (@trozos){
    	$secuencia_rand=$secuencia_rand.$_;
    }   
    return $secuencia_rand;
	}
sub pseudoreverse_trypsinKRP{
		my ($seq,$enzyme)= @_;
		my $enzyme_regex;
		my $protein_pseudorev="";
    	$enzyme_regex='(?<=[KR])(?=[^P])';
    	my @peptides=split(qr/$enzyme_regex/, $seq);
    	################################	Process of First peptide
    	my $first_pep=$peptides[0];
    	if((length $first_pep) >2 ){
    		$first_pep=~s/KP/1/g;
    		$first_pep=~s/RP/2/g;
    		my $last_aa= substr $first_pep,-1,1,'';
    		$first_pep= reverse $first_pep;
    		$first_pep.=$last_aa;
    		$first_pep=~s/1/KP/g;
    		$first_pep=~s/2/RP/g;
    	}
    	$protein_pseudorev=$first_pep;
    	################################	 Process of the second, 
    	#third,.. (n-1) peptides, only if there are more than 2 peptides
    	if($#peptides>1){
    		for(my $i=1;$i<$#peptides;$i++){
				if((length $peptides[$i]) <3){
				}
				else{
					my $last_aa= substr $peptides[$i],-1,1,'';
					$peptides[$i]=~s/KP/1/g;
    				$peptides[$i]=~s/RP/2/g;
					if($peptides[$i]=~/P$/){
						if((length $peptides[$i]) >1){
							$peptides[$i]=~s/([^P])(.*)(.)$/$2$3$1/;
							$peptides[$i]=reverse $peptides[$i];
						}
					}
					else{
						$peptides[$i]= reverse $peptides[$i];
					}
					$peptides[$i]=~s/1/KP/g;
    				$peptides[$i]=~s/2/RP/g;	
					#$peptides[$i]=~s/PK/KP/g;
					#$peptides[$i]=~s/PR/RP/g;	
					$peptides[$i].=$last_aa;				
				}
				$protein_pseudorev=$protein_pseudorev.$peptides[$i];
    		}
    	}
    	################################	Process last peptide, only if 
    	# there are at least two peptides	
    	if($#peptides>0){
			my $last_pep=$peptides[$#peptides];
			$last_pep=~s/KP/1/g;
    		$last_pep=~s/RP/2/g;
			if($last_pep=~/[KRP]$/){
					if((length $last_pep) >1){
						$last_pep=~s/([KRP]+)$//;
						my $tail=$1;
						$last_pep=reverse $last_pep;
						$last_pep=$last_pep.$tail;
					}
			}
			else{
				$last_pep=reverse $last_pep;
			}
			$last_pep=~s/1/KP/g;
    		$last_pep=~s/2/RP/g;
			$protein_pseudorev=$protein_pseudorev.$last_pep;
    	}
    return $protein_pseudorev;
}
sub pseudoreverse{
	my ($seq,$enzyme_regex)= @_;
	my $protein_pseudorev;
	my @peptides=split(qr/$enzyme_regex/, $seq);
	foreach my $pep(@peptides){
		my @aas= split //,$pep;
		my $aa_last= pop @aas;
		my $aas=join ('',@aas);
		$aas= reverse $aas;
		$protein_pseudorev.=$aas.$aa_last;
		}
	return $protein_pseudorev;
}

##########################		End of Decoy functionality		    #############################
#################################################################################################
#################################################################################################

#################################################################################################
#################################################################################################
##########################		Search functionality				#############################

sub search{
	my( $this, $event ) = @_;
	
	$this->{text_ctrl_1}->Clear;
	my $to_search= $this->{text_ctrl_5}->GetValue;#String or list of strings to search
	my $fasta_find=$this->{text_ctrl_4}->GetValue;#Fasta file to search in
	my $path_out= $this->{text_ctrl_6}->GetValue;#Name of the out file
	my $regex_search= $this->{checkbox_3}->GetValue;#Values: 1 or empty string
	my $mode_operation= $this->{combo_box_3}->GetValue;#Three modes of operation avaliable in the combo
	#What happens if no Fasta file is selected
	unless (-T $fasta_find){
			$this->{text_ctrl_1}->Clear;
			$this->{text_ctrl_1}->AppendText("Please, select a Fasta protein database using the ". 
			"\'Browse\'\nbutton in the left upper corner of the application.");
			return;
	}
	#Generation of the complete paths of out file
	my $out_dir=dir_from_file($fasta_find);
	
	unless($out_dir){
		$this->{text_ctrl_1}->Clear;
		$this->{text_ctrl_1}->AppendText("Error while creating the out file"."\n");
		return;
	}
	$path_out=$out_dir.$path_out;

	#################		Parameters validation
		if($mode_operation=~/^1/){#Mode of operation retrieval
			$mode_operation=1;
		}
		elsif($mode_operation=~/^2/){
			$mode_operation=2;
		}
		elsif($mode_operation=~/^3/){
			$mode_operation=3;
		}
		elsif($mode_operation=~/^4/){
			$mode_operation=4;
		}
		
		my @list_to_search;#List to search validation
		if($to_search){			#Textfield not empty
			if(-T$to_search){	#If is a textfile
				open SEARCH, "<$to_search";
				while(<SEARCH>){
					$_=~s/\W//g;
					if($_=~/\w+/){
						push @list_to_search, $_;
					}
				}
				close SEARCH;
			}
			else{					#or is a single string
				if($to_search=~/\w+/){
						push @list_to_search, $to_search;
					}
			}
		}
		else{
			$this->{text_ctrl_1}->AppendText("You must set a value to search\n");
			return;
		}
		if(-f$path_out){# If outfile exists, a messagebox Yes/No is displayed
			my $answer=Wx::MessageBox( "There is a file with the name $path_out in your application path\n" .
					  "Do you want to overwrite it?\n" . "",
					  "File exists", wxYES_NO|wxCENTRE|wxICON_QUESTION , $this );
		
			if($answer==wxYES){
				unlink $path_out;
			}
			else{
				$this->{text_ctrl_1}->AppendText("Please, choose other name in the 'Out Name' field for DB search. ");
				return;
			}
		}

	
	#Fasta lines prediction
	$this->{frame_1_statusbar}->SetStatusText('Progress: Loading the file.');
	my $lines_predicted= test_file_Fasta($fasta_find);
	unless ($lines_predicted){
		$this->{text_ctrl_1}->Clear;
		$this->{text_ctrl_1}->AppendText("Incorrect Fasta file\n");
		return;
	}

	##################		end of parameters validation
	
	##########		SEARCH BEGINS
	my $t0=time();
	$this->{text_ctrl_1}->AppendText("The following strings are going to be searched:\n");
	foreach(@list_to_search){
		$this->{text_ctrl_1}->AppendText("$_\n");
	}
	$this->{text_ctrl_1}->AppendText("---- A total of ".(1+$#list_to_search)." strings to search----\n");
	
	
	
	my ($prots_searched,$matches);
	
	if($mode_operation==1){
		($prots_searched,$matches)=&search_headers(\@list_to_search,$regex_search,
		$fasta_find,$path_out,$lines_predicted,$this);
		}
	elsif($mode_operation==2){				
	($prots_searched,$matches)=&search_sequences_generate_subDB(\@list_to_search,
	$regex_search,$fasta_find,$path_out,$lines_predicted,$this);
		}
	elsif($mode_operation==3){		
		($prots_searched,$matches)=&search_sequences_generate_peptide_prots_pairs(\@list_to_search,
		$regex_search,$fasta_find,$path_out,$lines_predicted,$this);
		}
	elsif($mode_operation==4){		
		($prots_searched,$matches)=&search_sequences_generate_protein_peptides_pairs(\@list_to_search,
		$regex_search,$fasta_find,$path_out,$lines_predicted,$this);
		}	
	############		END OF SEARCH
	$this->{text_ctrl_1}->AppendText("\nSearch complete.\n A file $path_out \n has been generated with the results\n");
	$this->{text_ctrl_1}->AppendText("A total of $prots_searched proteins have been scanned, and $matches matches have taken place\n");	
	my $tf=time();
	my $t=$tf-$t0;
	$this->{text_ctrl_1}->AppendText("Total time: $t secs \n");
	$this->{frame_1_statusbar}->SetStatusText('Ready');	
	
		
	}

sub search_headers{
	my($strings,$block,$file,$file_out,$lines_predicted,$this)=@_;
	my @strings_search=@{$strings};
	my $cont_prot=0;
	my $match=0;
	my $flag_next_sequence=0;
	my $sec="";
	open ARX, "<$file";
	open ARX2, ">$file_out"; 
	##########	Create progress milestones	
	my @ten_line_numbers;
	push @ten_line_numbers,0;
	for(my $i=1;$i<101;$i++){
		push @ten_line_numbers,(($lines_predicted/100)*$i);
	}
	#########
	my $progress_num=1;
	my $line=0;
	while (my $linea=<ARX>){
		$line++;
		if ($linea=~/^\>/){
			if ($progress_num<99){
				if($line>$ten_line_numbers[$progress_num]){
					$progress_num++;
					$this->{frame_1_statusbar}->SetStatusText('Progress: '.$progress_num.' %');
					$this->Refresh();
					$this->Update();
				}
			}
			elsif($progress_num==99){
				$progress_num++;
				$this->{frame_1_statusbar}->SetStatusText('Progress: 99%');
			}
			if($flag_next_sequence){#if the match was at the previous header, the sequence is printed
				my @aas= split //,$sec;
				my $count=0;
				foreach(@aas){
					$count++;
					print ARX2 $_;
					if ($count==60){
						$count=0;
						print ARX2 "\n";
						}
					}
				unless($count==0){#new line if the last line was 60 aa's long
					print ARX2 "\n";
					}	
				$flag_next_sequence=0;
				}
			unless($block){#No Perl REGEX wildcards
				foreach my $string(@strings_search){
					if($linea=~/\Q$string\E/){#here, the match is at the header
						print ARX2 $linea;
						$flag_next_sequence=1;
						$match++;
					}
				}	
			}
			else{##Perl REGEX wildcards
				foreach my $string(@strings_search){
					if($linea=~/$string/){#here, the match is at the header
						print ARX2 $linea;
						$flag_next_sequence=1;
						$match++;
					}	
				}
			}		
			$cont_prot++;
			$sec="";
			}
		else{
			$linea=~s/\W//g;
			$sec.=$linea;
			}	
	}
	
	if($flag_next_sequence){#This covers the case when the last header matches the search
		print ARX2 $sec;
	}
	close ARX;
	close ARX2;
	return ($cont_prot,$match);
}	


sub search_sequences_generate_subDB{
	my($strings,$block,$file,$file_out,$lines_predicted,$this)=@_;
	my @strings_search=@{$strings};
	my $cont_prot=0;
	my $match=0;
	my $header="";
	my $sec="";
	open ARX, "<$file";
	open ARX2, ">$file_out";
	##########	Create progress milestones	
	my @ten_line_numbers;
	push @ten_line_numbers,0;
	for(my $i=1;$i<101;$i++){
		push @ten_line_numbers,(($lines_predicted/100)*$i);
	}
	#########
	my $progress_num=1;
	my $line=0;
	LINE: while (my $linea=<ARX>){
		$line++;
		if ($linea=~/^\>/){
			if ($progress_num<99){
				if($line>$ten_line_numbers[$progress_num]){
					$progress_num++;
					$this->{frame_1_statusbar}->SetStatusText('Progress: '.$progress_num.' %');
					$this->Refresh();
					$this->Update();
				}
			}
			elsif($progress_num==99){
				$progress_num++;
				$this->{frame_1_statusbar}->SetStatusText('Progress: 99%');
			}
			my $matched=0;
			unless($block){#No Perl REGEX wildcards
				foreach my $string(@strings_search){
					if($sec=~/\Q$string\E/){#here, the match is at the sequence
					$matched=1;				
					}
				}
			}
			else{#Perl REGEX wildcards
				foreach my $string(@strings_search){
					if($sec=~/$string/){#here, the match is at the sequence
					$matched=1;				
					}
				}
			}
			if($matched){
				print ARX2 $header;
				my @aas= split //,$sec;
				my $count=0;
				foreach(@aas){
					$count++;
					print ARX2 $_;
					if ($count==60){
						$count=0;
						print ARX2 "\n";
					}
				}
				unless($count==0){
					print ARX2 "\n";
				}
				$match++;		
			}
				
			$cont_prot++;
			$sec="";#sequence is emptied
			$header=$linea;#the header is stored, in the hope the next sequence will match 
		}
		else{
			$linea=~s/\W//g;
			$sec.=$linea;
		}	
	}
	close ARX;
	close ARX2;						
	return ($cont_prot,$match);
}

sub search_sequences_generate_peptide_prots_pairs{
	my($strings,$block,$file,$file_out,$lines_predicted,$this)=@_;
	my @strings_search=@{$strings};
	my $cont_prot=0;
	my $match=0;
	my $header="";
	my $sec="";
	open ARX, "<$file";
	open ARX2, ">$file_out";
	my %matches;
	##########	Create progress milestones	
	my @ten_line_numbers;
	push @ten_line_numbers,0;
	for(my $i=1;$i<101;$i++){
		push @ten_line_numbers,(($lines_predicted/100)*$i);
	}
	#########
	my $progress_num=1;
	my $line=0;
	while (my $linea=<ARX>){
		$line++;
		if ($linea=~/^\>/){
			if ($progress_num<99){
					if($line>$ten_line_numbers[$progress_num]){
						$progress_num++;
						$this->{frame_1_statusbar}->SetStatusText('Progress: '.$progress_num.' %');
						$this->Refresh();
						$this->Update();
					}
			}
			elsif($progress_num==99){
					$progress_num++;
					$this->{frame_1_statusbar}->SetStatusText('Progress: 99%');
			}
			my $dif_pep="";
			unless($block){#No Perl REGEX wildcards
				foreach my $string(@strings_search){
					if($sec=~/\Q$string\E/){#here, the match is at the sequence, REGEX wildcards blocked
						$dif_pep=$string;
						if(defined $matches{$dif_pep}){
							$matches{$dif_pep}=$matches{$dif_pep}.'@@@@'.$header;
							}
						else{
							$matches{$dif_pep}=$header;	
							}	
						$match++;				
					}
				}
			}
			else{#Perl REGEX wildcards
				foreach my $string(@strings_search){
					if($sec=~/($string)/){#here, the match is at the sequence
						$dif_pep=$1;#if a regex wildcard is used, the matches value is stored and used as peptide
						if(defined $matches{$dif_pep}){
							$matches{$dif_pep}=$matches{$dif_pep}.'@@@@'.$header;
							}
						else{
							$matches{$dif_pep}=$header;	
							}	
						$match++;				
					}
				}	
			}	
			$cont_prot++;
			$sec="";#sequence is emptied
			$header=$linea;#the header is stored, in the hope the next sequence will match 
		}
		else{
			$linea=~s/\W//g;
			$sec.=$linea;
		}	
	}
	foreach  my $pep(keys %matches){
		my @prots= split /@@@@/,$matches{$pep};
		foreach my $prot(@prots){
			print ARX2 $pep."\t".$prot;
			}	
	}
	close ARX;
	close ARX2;
	return ($cont_prot,$match);
}

sub search_sequences_generate_protein_peptides_pairs{
	my($strings,$block,$file,$file_out,$lines_predicted,$this)=@_;
	my @strings_search=@{$strings};
	my $cont_prot=0;
	my $match=0;
	my $header="";
	my $sec="";
	open ARX, "<$file";
	open ARX2, ">$file_out";
	my %matches;
	##########	Create progress milestones	
	my @ten_line_numbers;
	push @ten_line_numbers,0;
	for(my $i=1;$i<101;$i++){
		push @ten_line_numbers,(($lines_predicted/100)*$i);
	}
	#########
	my $progress_num=1;
	my $line=0;
	while (my $linea=<ARX>){
		$line++;
		if ($linea=~/^\>/){
			if ($progress_num<99){
				if($line>$ten_line_numbers[$progress_num]){
					$progress_num++;
					$this->{frame_1_statusbar}->SetStatusText('Progress: '.$progress_num.' %');
					$this->Refresh();
					$this->Update();
				}
			}
			elsif($progress_num==99){
				$progress_num++;
				$this->{frame_1_statusbar}->SetStatusText('Progress: 99%');
			}
			$linea=~s/\n//g;
			$linea=~s/\r//g;
			my $dif_pep="";
			unless($block){#No Perl REGEX wildcards
				foreach my $string(@strings_search){
					if($sec=~/\Q$string\E/){#here, the match is at the sequence, REGEX wildcards blocked
						$dif_pep=$string;
						if(defined $matches{$header}){
							$matches{$header}=$matches{$header}."\t".$dif_pep;
							}
						else{
							$matches{$header}=$dif_pep;	
							}	
						$match++;				
					}
				}
			}
			else{#Perl REGEX wildcards
				foreach my $string(@strings_search){
					if($sec=~/($string)/){#here, the match is at the sequence
						$dif_pep=$1;#if a regex wildcard is used, the matches value is stored and used as peptide
						if(defined $matches{$header}){
							$matches{$header}=$matches{$header}."\t".$dif_pep;
							}
						else{
							$matches{$header}=$dif_pep;	
							}	
						$match++;				
					}
				}	
			}	
			$cont_prot++;
			$sec="";#sequence is emptied
			$header=$linea;#the header is stored, in the hope the next sequence will match 
		}
		else{
			$linea=~s/\W//g;
			$sec.=$linea;
		}	
	}
	foreach  my $prot(keys %matches){
		print ARX2 $prot."\t".$matches{$prot}."\n";	
	}
	close ARX;
	close ARX2;
	return ($cont_prot,$match);
}

##########################		End of Search functionality		    #############################
#################################################################################################
#################################################################################################

#################################################################################################
#################################################################################################
##########################		Join functionality				#############################











##########################		End of Join functionality		    #############################
#################################################################################################
#################################################################################################

#########################################################################################################
#########################################################################################################
##########################	Modules used in all functionalities		#####################################
sub test_file_Fasta{																					#
	#Tests if a string is a text file, and returns a prediction of fasta								#
	#file size according to the empirical formula lines_Fasta=(Size_in_Mb + 1.7858)/0.00007				#
	my $test=shift;																						#
	if(-T $test){
		#Test if the first or second line contains a > simbol at the beginning of line
		open TEST,"<$test";			
		my $count_lines=0;
		my $test_fasta_simbol=0;
		my $test_fasta_header=0;
		my $test_sequence=0;
		while(<TEST>){
			$count_lines++;
			if($_=~/^>/){
				$test_fasta_simbol=1;
				my @header_parts= split />/,$_;
				if($header_parts[1]){
					$test_fasta_header=1;	
				}
			}
			else{#Verify the existence of a sequence
				if($_=~/\w/){
					$test_sequence=1;	
				}	
			}
		}

		close TEST;
		unless($test_fasta_simbol and $test_fasta_header and $test_sequence){
			return;	
		}
		return $count_lines;
	}
}

sub test_file_Fasta2{																					#
	#Tests if a string is a text file, and returns a prediction of fasta								#
	#file size according to the empirical formula lines_Fasta=(Size_in_Mb + 1.7858)/0.00007				#
	my $test=shift;																						#
	if(-T $test){
		#Test if the first or second line contains a > simbol at the beginning of line
		open TEST,"<$test";			
		my $count_lines=0;
		my $test_fasta_simbol=0;
		my $test_fasta_header=0;
		my $test_sequence=0;
		while(<TEST>){
			$count_lines++;
			if($_=~/^>/){
				$test_fasta_simbol=1;
				my @header_parts= split />/,$_;
				if($header_parts[1]){
					$test_fasta_header=1;	
				}
			}
			else{#Verify the existence of a sequence
				if($_=~/\w/){
					$test_sequence=1;	
				}	
			}
			if($count_lines>100){
				last;
			}
		}
		close TEST;

		unless($test_fasta_simbol and $test_fasta_header and $test_sequence){
			return;	
		}	
		#predict de number of lines of Fasta file
		my $filesize = -s $test;
		$filesize=$filesize/1000000;
		my $lines_calc =($filesize+1.7858)/0.00007;
		$lines_calc= sprintf ("%.0f",$lines_calc);
		return $lines_calc;
	}
}


sub dir_from_file{
	my $file= shift;
	my @parts_file1= split /\\/,$file;#win32
	my @parts_file2= split /\//,$file;#linux
	pop @parts_file1;
	pop @parts_file2;
	my ($possible_dir1,$possible_dir2);
	$possible_dir1= join '\\',@parts_file1;#win32
	$possible_dir2= join '/',@parts_file2;#linux
	$possible_dir1.='\\';
	$possible_dir2.='/';
	if(-d $possible_dir1){#win32
		return $possible_dir1;
		}
	elsif(-d $possible_dir2){#linux
		return $possible_dir2;
		}
	else{
		return;
		}
	
}

sub mass_peptide{
	# The peptide mass is obtained, used a hash, globally declared (%masses).
	# The argument is a peptide, then is splitted into aminoacids, which are
	# the keys of %masses. For aminoacids different to the 20 canonical	a compromise
	# mass of 100 is used.
	#The peptide mass is returned
	my @aminoacids= split //,$_[0];
	my $mass=0;
	foreach(@aminoacids){
		unless(defined $masses{$_}){# Aminoacids different from the 20 canonical-> X,U,...
			$mass+=100;
			next;
		}
		$mass+=$masses{$_};
		}
	return $mass;
}

sub format_commas{
	my $num=$_[0];
	my $i=0;
	while ($i<5){#This loop while generate a maximum of 5 commas (10^15)
		$num=~s/(.*)(\d)(\d\d\d)/$1$2,$3/;																	
		$i++;																							#
		}																								#
	return $num;																						#
}																										#
#########################################################################################################
#########################################################################################################



